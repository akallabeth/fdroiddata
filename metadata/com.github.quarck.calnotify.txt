Categories:System,Time
License:GPLv3
Web Site:
Source Code:https://github.com/quarck/CalendarNotification
Issue Tracker:https://github.com/quarck/CalendarNotification/issues

Auto Name:Calendar Notifications Plus
Summary:Advanced calendar notifications
Description:
Replace calendar notifications with advanced notifications, allowing custom
snooze times and reboot notifications persistence. The app is focusing on making
its functionality as transparent as possible, notification would behave in the
same way as stock calendar notifications. All calendar applications should be
handled correctly in theory.
.

Repo Type:git
Repo:https://github.com/quarck/CalendarNotification

Build:1.0.1,2
    commit=ed8f05c87f90e982885e9198999edb7fbe92612e
    subdir=app
    gradle=yes

Build:1.0.3,5
    commit=Release_1.0.3
    subdir=app
    gradle=yes

Build:1.0.9,10
    commit=Release_1.0.9
    subdir=app
    gradle=yes

Build:1.0.10,11
    commit=Release_1.0.10
    subdir=app
    gradle=yes

Auto Update Mode:Version Release_%v
Update Check Mode:Tags ^Release
Current Version:1.0.10
Current Version Code:11
